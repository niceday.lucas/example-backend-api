package io.niceday.common.coffee.common.maker;

public interface Maker {

	void generate(Target target) throws Exception;
}