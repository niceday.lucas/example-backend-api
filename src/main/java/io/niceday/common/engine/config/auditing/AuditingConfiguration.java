package io.niceday.common.engine.config.auditing;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description auditing configuration
 **********************************************************************************************************************/
@Configuration 
@EnableJpaAuditing
public class AuditingConfiguration { 
	
}
